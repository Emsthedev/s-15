console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

 console.log(
    "Start of Activity Part 1"
 )
 console.log("");
    
let myFirstName = "Emmanuel Fernando";
let myLastName ="Garcia";
let myAge = 24;
let myHobbies = [" Playing Music "," Playing Basketball "," Playing Mobile Legends "];

const myAddress = {

    houseNumber: "01",
    street: "Arcadia Street",
    city: "Mandaluyong",
    state: "Metropolitan Manila"


};

console.log("My First Name: "+myFirstName);
console.log("My Last Name: "+myLastName);
console.log("My Age is: "+ myAge + " yrs old");
console.log("Here are my hobbies listed in an array: " + myHobbies);
console.log("My Address is listed below in a Object:" + myAddress);
console.log(myAddress);
console.log("");

console.log("End of Activity Part 1")


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
console.log("");

console.log(
    "Start of Activity Part 2"
 )
 console.log("");

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: " + friends)
	

	let profile = {

		username: "captain_america",
		fullName2: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	const lastLocation = "Arctic Ocean";
	let lastLocation2 = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

    console.log("");
   
    console.log("End of Activity Part 2")

    console.log("");